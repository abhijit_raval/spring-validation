<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
.error {
	color: #ff0000;
}
</style>
<body>
	<center>
		<form:form method="POST" action="intro" modelAttribute="employee">
			<table>
				<tr>
					<td><form:label path="name">Name</form:label></td>
					<td><form:input path="name" /></td>
					<td><form:errors path="name" cssClass="error" /></td>
				</tr>

				<tr>
					<td><form:label path="pass">Password</form:label></td>
					<td><form:input path="pass" /></td>
					<td><form:errors path="pass" cssClass="error" /></td>
				</tr>
				<tr>
					<td><form:label path="email">email</form:label></td>
					<td><form:input path="email" /></td>
					<td><form:errors path="email" cssClass="error" /></td>
				</tr>
				<tr>
					<td><form:label path="age">Age</form:label></td>
					<td><form:input path="age" /></td>
					<td><form:errors path="age" cssClass="error" /></td>
				</tr>
				<tr>
					<td><form:label path="country">Country</form:label></td>
					<td><form:select path="country">
							<form:option value="NONE" label="Select" />
							<form:options items="${countryList}" />
						</form:select></td>
					<td><form:errors path="country" cssClass="error" /></td>
				</tr>
				<tr align="center">
					<td colspan="2"><input type="submit" value="Submit" /></td>
				</tr>
			</table>
		</form:form>
	</center>
</body>
</html>