package com.validator.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.validator.model.Employee;

@Controller

public class HomeController {

	@GetMapping("/intro")
	ModelAndView intro() {
		return new ModelAndView("Intro", "employee", new Employee());
	}

	@ModelAttribute("employee")
	public Employee createStudentModel() {
		return new Employee();
	}

	@PostMapping("/intro")
	String submitForm(@ModelAttribute("employee") @Validated Employee e, BindingResult br, Model model) {

		if (br.hasErrors()) {
			System.out.println("In error");
			return "Intro";
		} else {
			System.out.println("In success");
			return "redirect:success";
		}
	}

	@GetMapping("/success")
	String success() {
		return "success";
	}

	@ModelAttribute("countryList")
	public Map<String, String> getCountries() {
		Map<String, String> countries = new HashMap<String, String>();
		countries.put("US", "United States");
		countries.put("IN", "India");
		countries.put("SL", "Sri lanka");
		countries.put("MY", "Malaysia");
		return countries;
	}

}
